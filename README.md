# weet

A true minimalist theme without a header and without default WordPress styles, that puts content first.

The theme’s CSS is inlined in the HTML output and it uses no JS for maximum performance.

Default stylesheets can be enabled in the theme options.

The theme is compatible with:

- [WordPress](https://wordpress.org/), version 4.9 or later
- [ClassicPress](https://www.classicpress.net/)

## Screenshot

![Screenshot](screenshot.png "Screenshot of a webpage using the weet theme")

## Downloads

Download is available on [wordpress.org](https://wordpress.org/themes/weet/).

You can also install **weet** with [Composer](https://getcomposer.org/).

```
composer require --no-dev wee/weet
```

## Author

[Walter Ebert](https://wee.press)

## License

[GPL 2.0 or later](https://spdx.org/licenses/GPL-2.0-or-later.html)

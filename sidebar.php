<?php
/**
 * Site sidebar
 *
 * @package WordPress
 * @subpackage weet
 * @since 2.1.0
 */

if ( is_active_sidebar( 'widgets' ) ) {
	echo '<aside id="widgets">';
	dynamic_sidebar( 'widgets' );
	echo '</aside>';
}

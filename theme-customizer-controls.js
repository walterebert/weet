/**
 * Theme Customizer controls.
 */

( function ( wp ) {
	wp.customize.bind( 'ready', function() {
		wp.customize( 'show_author', function( show_author ) {
			var url = wp.customize.control( 'add_author_url' ).container;
			if( show_author.get() ) {
				url.show();
			} else {
				url.hide();
			}

			show_author.bind( function( checked ) {
				if( checked ) {
					url.show();
				} else {
					url.hide();
				}
			} );
		} );
	} );
} )( window.wp );

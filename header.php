<?php
/**
 * Page header
 *
 * @package WordPress
 * @subpackage weet
 * @since 1.0.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php echo esc_attr( get_bloginfo( 'charset' ) ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php
if ( function_exists( 'wp_body_open' ) ) {
	wp_body_open();
}
?>
<div id="page">

	<header><a href="#site-navigation" class="skiplink"><?php echo __( 'Go to navigation', 'weet' ); ?></a></header>

	<?php if ( is_singular() ) : ?>
		<main id="content">
	<?php else: ?>
		<main id="content" class="h-feed">
	<?php endif; ?>

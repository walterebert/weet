=== weet ===
Contributors: walterebert
Tags: accessibility-ready, blog, microformats, one-column, sticky-post, theme-options, threaded-comments, translation-ready
Requires at least: 4.9
Tested up to: 6.6
Requires PHP: 5.4
Version: 2.3.0
License: GPL-2.0-or-later
License URI: https://spdx.org/licenses/GPL-2.0-or-later.html

A true minimalist theme without a header and without default WordPress styles, that puts content first.

== Description ==

weet is a theme that focuses on speed and puts content first. It doesn’t have a
header section and the WordPress stylesheets are not loaded by default.

The theme’s CSS is inlined in the HTML output and it uses no JS for maximum
performance.

The WordPress Core stylesheets can be enabled in the theme options, if you really
want them.

The post meta information, like date and author can be customized by defining
the PHP function `weet_entry_meta`.

The post terms, like tags and categories tags can be customized by defining
the PHP function `weet_terms_list`.

== Copyright ==
weet, Copyright 2022-2024 Walter Ebert
weet is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

== Changelog ==

= 2.3 =
- Added ClassicPress detection

= 2.2 =
- Removed additional core styles
- Improved compatibility with HTML processors

= 2.1 =
- Added post password protection.
- Added widget area.
- Added accessibility improvements.
- Added heading to front, archive, search pages.
- Improved showing comments.
- Updated CSS.

= 2.0 =
- Added option to list tags and categories.
- Added option to list the author name.
- Added option to enable default styles.
- Added date and author to posts.
- Added skip link.
- Improved Microformats 2 support.
- Improved conformance to coding standards.

= 1.1 =
Updated CSS and theme information.

= 1.0 =
Initial release

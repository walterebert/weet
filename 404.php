<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage weet
 * @since 1.0.0
 */

get_header();
?>

<article id="page-not-found">
	<div class="entry-title">
		<h1><?php echo esc_html( 'Page not found', 'weet' ); ?></h1>
	</div>
	<div class="entry-content">
		<p><a href="<?php echo esc_url( home_url() ) ; ?>"><?php echo __( 'Visit the homepage', 'weet' ); ?></a></p>
	</div>
</article>

<?php
get_footer();

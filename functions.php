<?php
/**
 * Theme functions
 *
 * @package WordPress
 * @subpackage weet
 * @since 1.0.0
 */

/**
 * Customize comment form defaults
 *
 * @param array $fields Comment form fields
 * @return array
 * @since 2.0.2
 */
function weet_comment_form_defaults( $fields ) {
	$fields['title_reply_before'] = '<h2 id="reply-title" class="comment-reply-title">';
	$fields['title_reply_after'] = '</h2>';

	return $fields;
}
add_filter( 'comment_form_defaults', 'weet_comment_form_defaults', 10, 1 );

/**
 * Customize theme options
 *
 * @param object $wp_customize WordPress Customizer instance.
 * @since 2.0.0
 */
function weet_customize( $wp_customize ) {
	$wp_customize->add_section(
		'weet',
		array(
			'title' => __( 'Theme Options', 'weet' ),
		)
	);

	// WordPress default styles setting.
	if ( ! function_exists( 'classicpress_version' ) ) {
		$wp_customize->add_setting(
			'enable_default_styles',
			array(
				'type'              => 'theme_mod',
				'transport'         => 'refresh',
				'sanitize_callback' => 'sanitize_key',
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'enable_default_styles',
				array(
					'label'       => __( 'Enable default CSS', 'weet' ),
					'description' => __( 'Load all default WordPress CSS styles.', 'weet' ),
					'section'     => 'weet',
					'settings'    => 'enable_default_styles',
					'type'        => 'checkbox',
				)
			)
		);
	}

	// Author setting.
	$wp_customize->add_setting(
		'show_author',
		array(
			'type'              => 'theme_mod',
			'transport'         => 'refresh',
			'sanitize_callback' => 'sanitize_key',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'show_author',
			array(
				'label'       => __( 'Show author', 'weet' ),
				'description' => __( 'List the author name in blog posts.', 'weet' ),
				'section'     => 'weet',
				'settings'    => 'show_author',
				'type'        => 'checkbox',
			)
		)
	);

	// Author URL setting.
	$wp_customize->add_setting(
		'add_author_url',
		array(
			'type'      => 'theme_mod',
			'transport' => 'refresh',
			'sanitize_callback' => 'sanitize_key',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'add_author_url',
			array(
				'label'       => __( 'Add author page link', 'weet' ),
				'description' => __( 'Add a link to the author page in blog posts.', 'weet' ),
				'section'     => 'weet',
				'settings'    => 'add_author_url',
				'type'        => 'checkbox',
			)
		)
	);

	// Tags setting.
	$wp_customize->add_setting(
		'show_tags',
		array(
			'type'              => 'theme_mod',
			'transport'         => 'refresh',
			'sanitize_callback' => 'sanitize_key',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'show_tags',
			array(
				'label'       => __( 'Show tags', 'weet' ),
				'description' => __( 'List the tags in blog posts.', 'weet' ),
				'section'     => 'weet',
				'settings'    => 'show_tags',
				'type'        => 'checkbox',
			)
		)
	);

	// Categories setting.
	$wp_customize->add_setting(
		'show_categories',
		array(
			'type'              => 'theme_mod',
			'transport'         => 'refresh',
			'sanitize_callback' => 'sanitize_key',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'show_categories',
			array(
				'label'       => __( 'Show categories', 'weet' ),
				'description' => __( 'List the categories in blog posts.', 'weet' ),
				'section'     => 'weet',
				'settings'    => 'show_categories',
				'type'        => 'checkbox',
			)
		)
	);
}
add_action( 'customize_register', 'weet_customize', 10, 1 );

/**
 * Load Customizer controls
 *
 * @since 2.0.0
 */
function weet_customize_controls() {
	wp_enqueue_script(
		'weet_customize_controls',
		get_stylesheet_directory_uri() . '/theme-customizer-controls.js',
		array( 'jquery' ),
		wp_get_theme()->get( 'Version' ),
		true
	);
}
add_action( 'customize_controls_enqueue_scripts' , 'weet_customize_controls' , 10, 0 );

if ( ! function_exists( 'weet_entry_meta' ) ) {
	/**
	 * Meta data for blog posts
	 *
	 * @return string
	 * @since 2.0.0
	 */
	function weet_entry_meta() {
		$separator = '<span class="entry-meta-separator" aria-hidden="true">&middot;</span>';

		$html = '<div class="entry-meta"><ul>';

		// Publication date information.
		$publication_date = get_the_date( DATE_W3C );
		$html .= '<li class="entry-meta-published">'
			. '<span class="screen-reader-text">' . __( 'Posted: ', 'weet' ) . '</span>'
			. '<time class="dt-published" datetime="' . esc_attr( $publication_date ) . '">'
			. esc_html( get_the_date() )
			. '</time>';

		// Modification date information.
		$modification_date = get_the_modified_date( DATE_W3C );
		if ( $modification_date && $publication_date !== $modification_date ) {
			$html .= '<li class="entry-meta-updated">'
				. $separator
				. '<span class="screen-reader-text">' . __( 'Updated: ', 'weet' ) . '</span>'
				. '<time class="dt-updated" datetime="' . esc_attr( $modification_date ) . '">'
				. esc_html( get_the_modified_date() )
				. '</time>';
		}

		// Author name.
		$show_author = get_theme_mod( 'show_author' );
		if ( $show_author ) {
			$author = get_the_author();
			if ( $author ) {
				$html .= '<li class="entry-meta-author">'
					. $separator
					. '<span class="screen-reader-text">' . __( 'Author: ', 'weet' ) . '</span>';

				$add_author_url = get_theme_mod( 'add_author_url' );
				if ( $add_author_url ) {
					$author_url = get_the_author_meta( 'user_url' );
					if ( ! $author_url ) {
						$author_url = home_url( '/' );
					}
		
					$html .= '<a href="' . esc_url( $author_url ) . '" class="p-author">'
						. $author
						. '</a>';
				} else {
					$html .= '<span class="p-author">'
						. $author
						. '</span>';
				}
			}
		}

		// GUID.
		$html .= '<li class="u-uid entry-meta-guid" aria-hidden="true">' . esc_html( get_the_guid() );

		$html .= '</ul></div>';

		return $html;
	}
}

/**
 * Customize loading of stylesheets and scripts
 *
 * @since 1.0.0
 */
function weet_enqueue_scripts() {
	if ( ! function_exists( 'classicpress_version' ) ) {
		// List of WordPress Core stylesheet handles.
		$handles = array(
			'classic-theme-styles',
			'global-styles',
			'wp-block-library',
			'wp-components',
			'wp-emoji-styles',
		);

		// Disable WordPress Core styles.
		$enable_default_styles = get_theme_mod( 'enable_default_styles' );
		if ( empty( $enable_default_styles ) ) {
			foreach ( $handles as $handle) {
				wp_dequeue_style( $handle );
				wp_deregister_style( $handle );
			}
		}
	}

	// Threaded comments.
	if ( is_singular() && ( comments_open() || get_comments_number() ) && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'weet_enqueue_scripts', 10, 0 );

/**
 * Customize post_class filter
 *
 * @since 2.0.0
 */
function weet_post_class( $classes ) {
	if ( is_single() || ! is_singular() ) {
		$classes[] = 'h-entry';
	}

	return array_unique( $classes );
}
add_filter( 'post_class', 'weet_post_class', 10, 1 );

/**
 * Theme setup
 *
 * @since 1.0.0
 */
function weet_setup() {
	if ( ! function_exists( 'classicpress_version' ) ) {
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption', 'style', 'script', 'navigation-widgets' ) );

		// Enable default WordPress styles.
		$enable_default_styles = get_theme_mod( 'enable_default_styles' );
		if ( $enable_default_styles ) {
			add_theme_support( 'custom-line-height' );
			add_theme_support( 'wp-block-styles' );
			add_theme_support( 'responsive-embeds' );
			add_theme_support( 'custom-spacing' );
			add_theme_support( 'appearance-tools' );
		} else {
			remove_action( 'wp_enqueue_scripts', array( 'WP_Duotone', 'output_block_styles'), 9 );
			remove_action( 'wp_enqueue_scripts', array( 'WP_Duotone', 'output_global_styles' ), 11 );
			remove_action( 'wp_footer', array( 'WP_Duotone', 'output_footer_assets' ), 10 );
		}
	}

	add_theme_support( 'title-tag' );
	add_theme_support( 'automatic-feed-links' );

	// Enable styles in the editor.
	add_theme_support( 'editor-styles' );
	add_editor_style();

	// Site navigation menu.
	register_nav_menus(
		array(
			'primary' => esc_html__( 'Site navigation', 'weet' ),
		)
	);
}
add_action( 'after_setup_theme', 'weet_setup', 10, 0 );

/**
 * Load stylesheets inline
 * 
 * Improved support for HTML processors that do not support CSS:
 * https://tantek.com/2024/075/t1/css-more-robust-style-element
 *
 * @since 1.0.0
 */
function weet_styles() {
	echo '<style><!--/*--><![CDATA[*/' . PHP_EOL;
	echo file_get_contents( __DIR__ . '/weet.css' );
	echo '/*]]><!--*/--></style>' . PHP_EOL;
}
add_action( 'wp_head', 'weet_styles', 10, 0 );

/**
 * Get post terms by taxonomy
 *
 * @param string $taxonomy Post taxonomy
 * @return array Term links
 * @since 2.0.0
 */
function weet_terms( $taxonomy = 'post_tag' ) {
	global $post;

	$rel = 'tag';
	if ( 'category' === $taxonomy ) {
		$rel = 'category tag';
	}

	$links = array();

	$terms = get_the_terms( $post, $taxonomy );
	if ( ! is_array( $terms ) ) {
		return '';
	}
	foreach ( $terms as $term ) {
		$link = get_term_link( $term, $taxonomy );
		if ( is_string( $link ) ) {
			$links[] = '<a href="' . esc_url( $link ) . '" rel="' . esc_attr( $rel ) . '"><span aria-hidden="true">#</span><span class="p-category">' . $term->name . '</span></a>';
		}
	}

	return $links;
}

if ( ! function_exists( 'weet_terms_list' ) ) {
	/**
	 * Get post terms list (tags + categories)
	 *
	 * @return string HTML
	 * @since 2.0.0
	 */
	function weet_terms_list() {
		$html = '';

		// Tags.
		$tags_list = '';
		$show_tags = get_theme_mod( 'show_tags' );
		if ( $show_tags ) {
			$tags_terms = weet_terms( 'post_tag' );
			if ( $tags_terms ) {
				$tags_list = implode( ' ', $tags_terms );
			}
		}

		// Categories.
		$categories_list = '';
		$show_categories = get_theme_mod( 'show_categories' );
		if ( $show_categories ) {
			$categories_terms = weet_terms( 'category' );
			if ( $categories_terms ) {
				$categories_list = implode( ' ', $categories_terms );
			}
		}

		if ( $categories_list || $tags_list ) {
			$html .= '<nav class="entry-meta entry-meta-terms" aria-label="' . esc_attr( __( 'Post terms', 'weet' ) ) . '">';
			$html .= $tags_list;
			if ( $categories_list && $tags_list ) {
				$html .= ' ';
			}
			$html .= $categories_list . '</nav>' . PHP_EOL;
		}

		return $html;
	}
}

/**
 * Initialize widgets
 * @since 2.1.0
 */
function weet_widgets_init() {
	register_sidebar(
		array(
			'name'          => __( 'Widget Area', 'weet' ),
			'id'            => 'widgets',
			'description'   => __( 'Appears at the bottom of pages and posts', 'weet' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'weet_widgets_init', 10, 0 );

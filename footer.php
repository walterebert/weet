<?php
/**
 * Page footer
 *
 * @package WordPress
 * @subpackage weet
 * @since 1.0.0
 */

?>

	</main><!-- #content -->

	<?php get_sidebar(); ?>

	<footer id="site-footer">
	<?php if ( has_nav_menu( 'primary' ) ) : ?>
		<nav id="site-navigation" tabindex="-1" aria-label="<?php echo esc_attr( __( 'Site navigation', 'weet' ) ); ?>">
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'primary',
					'menu_id'        => 'primary-menu',
					'menu_class'     => 'nav-menu',
				)
			);
			?>
		</nav>
	<?php endif; ?>
	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

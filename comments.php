<?php
/**
 * Template for displaying Comments
 *
 * @package WordPress
 * @subpackage weet
 * @since 2.0.1
 */

if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && 'comments.php' === basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
	die( 'Please do not load this page directly. Thanks!' );
}
?>

<section class="comments">

<?php if ( have_comments() ) : ?>

	<h2 id="comments">
		<?php
		if ( 1 == get_comments_number() ) {
			printf(
				/* translators: %s: Post title. */
				__( 'One response to %s', 'weet' ),
				'&#8220;' . get_the_title() . '&#8221;'
			);
		} else {
			printf(
				/* translators: 1: Number of comments, 2: Post title. */
				_n( '%1$s response to %2$s', '%1$s responses to %2$s', get_comments_number(), 'weet' ),
				number_format_i18n( get_comments_number() ),
				'&#8220;' . get_the_title() . '&#8221;'
			);
		}
		?>
	</h2>

	<ol class="comments-list">
	<?php wp_list_comments(); ?>
	</ol>

	<?php
	$previous_comments_link = get_previous_comments_link();
	$next_comments_link = get_next_comments_link();
	if ( $previous_comments_link || $next_comments_link ) {
		echo '<nav class="comments-nav nav-links" aria-label="' . esc_attr( __( 'Comments pagination', 'weet' ) ) . '">';
		echo $previous_comments_link;
		echo $next_comments_link;
		echo '</nav>';
	}
	?>

<?php endif; ?>

<?php comment_form(); ?>

</section>

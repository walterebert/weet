<?php
/**
 * Default page.
 * 
 * @package WordPress
 * @subpackage weet
 * @since 1.0.0
 */

get_header();

if ( is_archive() ) {
	echo '<h1 class="p-name">' . get_the_archive_title() . '</h1>';
} elseif ( is_search() ) {
	echo '<h1 class="p-name">' . esc_html( __( 'Search results', 'weet' ) ) . '</h1>';
	get_search_form();
} elseif ( ! is_singular() ) {
	echo '<h1 class="p-name">';
	$single_post_title = single_post_title( '', false );
	if ( $single_post_title ) {
		echo $single_post_title;
	} else {
		bloginfo( 'name' );
	}
	echo '</h1>';
}
?>

<?php if ( have_posts() ) : ?>

	<?php while ( have_posts() ) : ?>

		<?php the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="entry-title">
				<?php
				if ( is_singular() ) {
					echo '<h1 class="p-name">' . get_the_title() . '</h1>';
				} else {
					echo '<h2><a href="' . esc_url( get_permalink() ) . '" class="p-name u-url">';
					echo get_the_title();
					echo '</a></h2>';
				}
				?>
			</div>

			<?php
			if ( post_password_required() ):
				echo get_the_password_form();
			else:
				if ( ! is_page() ) {
					echo weet_entry_meta();
				}
				?>

				<div class="entry-content e-content">
				<?php the_content(); ?>
				</div><!-- .e-content -->

				<?php
				wp_link_pages(
					array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'weet' ),
						'after'  => '</div>',
					)
				);

				if ( is_single() ) {
					echo weet_terms_list();
				}
				if ( is_singular() && ( comments_open() || get_comments_number() ) ) {
					comments_template( '', true );
				}
			endif;
			?>
		</article><!-- #post-## -->

	<?php endwhile; ?>

	<?php the_posts_navigation(); ?>

<?php else : ?>

	<p><?php esc_html_e( 'Nothing found', 'weet' ); ?>

<?php endif; ?>

<?php
get_footer();
